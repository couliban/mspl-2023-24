

## TD1: Setup

1. Download and Install **git** and **RStudio** (take the free version)
   - Git: https://git-scm.com/downloads
     - _Understand git and Tutorial_: https://try.github.io/
   - RStudio: https://www.rstudio.com/products/rstudio/download/
     for mac users you need to download R https://cran.r-project.org/
   - Note: an easier and faster way to install R, Python, Rstudio and Jupyter may be to install [Anaconda](https://www.anaconda.com/distribution/).

2. Login to your Gitlab account
   - Go to the login page: https://gricad-gitlab.univ-grenoble-alpes.fr
   - Using UGA Agalan login/password
   - _Documentation_: Have a look at the Gitlab documentation https://docs.gitlab.com/ee/gitlab-basics/README.html

3. Add your public SSH key
   - Add it here: https://gricad-gitlab.univ-grenoble-alpes.fr/-/profile/keys
   - _Tutorial_: https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html

4. Fork the reference repository
   - Fork this repository: https://gricad-gitlab.univ-grenoble-alpes.fr/MSPL/MSPL-2023-24
     - Please, use the `Fork` button in the web interface
   - _Tutorial_: https://docs.gitlab.com/ee/gitlab-basics/fork-project.html

5. Clone your forked repository locally
   - `git clone git@gricad-gitlab.univ-grenoble-alpes.fr:YOURLOGIN/MSPL-2023-24.git`
   - _Tutorial_: https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html


6. Create your first commit, then push it to gitlab
   - Create a folder `reports`
   - Create a file `reports/README.md` with your name, binôme, which git you and your binôme will upload the reports, the team name with the 4 students names and emails:
    ```
       - Student Name: Danilo Carastan dos Santos
         - Student Git https://gricad-gitlab.univ-grenoble-alpes.fr/carastda/mspl-2023-24
       - Binôme: Jean Marc Vincent
       - Binôme Git: https://gricad-gitlab.univ-grenoble-alpes.fr/jmv/MSPL-2023-24
       - Team Name: The Professors
         - Jean Marc Vincent - Jean-Marc.Vincent@univ-grenoble-alpes.fr
         - Danilo Carastan dos Santos - Danilo.Carastan-dos-Santos@univ-grenoble-alpes.fr
         - Member 3 - E-mail 3
         - Member 4 - E-mail 4
     ```
   - Then, run `git add reports/README.md`
   - Commit it locally using `git commit -m "my first commit"`
   - Push to github with `git push origin main`

7. Add MSPL-2023-24 remote

   Do in your local repository, the one that you've cloned, the following commands:
   - Run `git remote add MSPL-2023-24 git@gricad-gitlab.univ-grenoble-alpes.fr:MSPL/MSPL-2023-24.git`
   - Check for update and (merge or rebase) them locally by : `git pull MSPL-2023-24 main`
        for other choices have a look at fetch, merge and rebase commands
   - Push then to your gitlab repository: `git push origin main`

   Repeat the last two commands to keep your gitlab repository updated.

   This is necessary to keep yourself up to date with latest TD updates.

8. Install R packages. R has it's own package management mechanism so just run R and type the following commands:
   - `dplyr`, `tidyr` and `ggplot2` by Hadley Wickham [http://had.co.nz/](http://had.co.nz/)
    ```
    install.packages("dplyr")
    install.packages("tidyr")
    install.packages("ggplot2")
    ```

   - Or install a bundle of many usefull packages (that includes those three):
    ```
    install.packages("tidyverse")
    ```

   - `knitR` by Yihui Xie [http://yihui.name/knitr/](http://yihui.name/knitr/)
    ```
    install.packages("knitr")
    ```

Congratulations, try using the markdowns [2_ping-pong.Rmd](./2_ping-pong.Rmd) or [2_snow.Rmd](./2_snow.Rmd)!


## TD1: Alternatives

You may want to try alternatives to the RStudio notebooks and/or to the R programming language.

1. **Jupyter** is probably the most widely used software for notebooks. It supports many programming languages,
   including R, Python and Julia. Its [official website](https://jupyter.org/) tells you how to install and use it, you
   can even try one of their online demos without installing anything. Note that if you wish to use Jupyter, we strongly
   advise you to install it on your laptop and not rely on the online demo.
2. **Python** is a general-purpose programming language that can be used for doing statistics and data visualization.
   Its [official website](https://www.python.org/) tells you how to install it and provides some basic tutorials.

All the exercises of MSPL can be done with the R or Python programming language in a RStudio or Jupyter notebook. We
advise that you try both languages as they each have their own pros and cons.
